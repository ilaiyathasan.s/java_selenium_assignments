package com.seleniumassignments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DemoWebShopComputer {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		
		driver.findElement(By.linkText("Log in")).click();
		driver.findElement(By.id("Email")).sendKeys("misterthasan24@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("123456");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		
		WebElement computer=driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Computers')]"));
		Actions act = new Actions(driver);
		act.moveToElement(computer).build().perform();
		WebElement desktop=driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Desktops')]"));
		act.moveToElement(desktop).click().perform();
		
		Select sel = new Select(driver.findElement(By.id("products-orderby")));
		sel.selectByVisibleText("Price: Low to High");
		driver.findElement(By.xpath("//a[text()='Build your own cheap computer']")).click();
		driver.findElement(By.id("add-to-cart-button-72")).click();
		Thread.sleep(2000);
		driver.findElement(By.linkText("Shopping cart")).click();
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();
		

		driver.findElement(By.xpath("//input[@onclick='Billing.save()']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@onclick='Shipping.save()']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@onclick='ShippingMethod.save()']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@onclick='PaymentMethod.save()']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@onclick='PaymentInfo.save()']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@onclick='ConfirmOrder.save()']")).click();
		Thread.sleep(2000);
		WebElement order=driver.findElement(By.xpath("//li[contains(text(),'Order')]"));
		System.out.println(order.getText());
		
		driver.findElement(By.linkText("Log out")).click();
	}

}
